from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/registro', methods=['post', 'get'])
def registro():
    vendedor = request.form['vendedor']
    venta = request.form['venta']
    with open('registro.csv', 'a') as registro_ventas:
        registro_ventas.write(vendedor + "," + venta)
        comision = int(venta) * 0.05
        return render_template('registro.html', comision=comision)


if __name__ == '__main__':
    app.run(debug=True)